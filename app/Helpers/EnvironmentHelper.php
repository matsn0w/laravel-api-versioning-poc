<?php

namespace App\Helpers;

use Exception;
use App\Models\AppSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class EnvironmentHelper
{
    public static function default(): string
    {
        return AppSettings::first()->environment;
    }

    public static function getAll(): array
    {
        // scan the environments folder for .sqlite files
        $files = glob(database_path('/environments/*.sqlite'));

        // remove the file extensions
        $files = array_map(function ($file) {
            return basename($file, '.sqlite');
        }, $files);

        return $files;
    }

    public static function fork(string $environment, string $name): void
    {
        // get the environment path
        $env_path = self::getDatabaseFile($environment);

        // get the new path
        $new_path = database_path("/environments/$name.sqlite");

        // check if the new environment already exists
        if (file_exists($new_path)) {
            throw new Exception('This environment already exists!');
        }

        // copy the file
        $result = copy($env_path, $new_path);

        if (!$result) {
            throw new Exception('Something went wrong!');
        }

        // fork the schema file
        SchemaHelper::forkSchema($environment, $name);
    }

    public static function setDefault(string $environment): void
    {
        // check if the environment exists
        self::getDatabaseFile($environment);

        // update app database
        DB::connection('sqlite_app')->table('settings')->update(['environment' => $environment]);

        // update config
        self::setDatabaseConnection($environment);

        // purge old connection
        DB::purge('sqlite');
    }

    public static function delete(string $environment): void
    {
        // get the environment path
        $env_path = self::getDatabaseFile($environment);

        // check if the requested environment is the default
        if ($environment == self::default()) {
            throw new Exception('You can\'t delete the default environment!');
        }

        // delete the database file
        $result = unlink($env_path);

        if (!$result) {
            throw new Exception('Something went wrong!');
        }

        SchemaHelper::delete($environment);
    }

    public static function setDatabaseConnection(string $environment): void
    {
        // set the new environment as database connection
        Config::set('database.connections.sqlite.database', database_path("/environments/$environment.sqlite"));
    }

    protected static function getDatabaseFile(string $environment): string
    {
        // build the file path
        $path = database_path("/environments/$environment.sqlite");

        // check if the file exists
        $exists = file_exists($path);

        if (!$exists) {
            throw new Exception('This environment does not exist!');
        }

        return $path;
    }
}
