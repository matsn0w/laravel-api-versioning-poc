<?php

namespace App\Console\Commands;

use App\Helpers\EnvironmentHelper;
use Exception;
use Illuminate\Console\Command;

class ForkEnvironment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment:fork
                            {environment : The database to copy}
                            {name : The name of the new environment}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fork a database environment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $environemnt = $this->argument('environment');
        $name = $this->argument('name');

        try {
            EnvironmentHelper::fork($environemnt, $name);
        } catch (Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return 1;
        }

        $this->info('Environment forked successfully!');

        $default = $this->confirm('Do you want to set this environment as default?');

        if ($default) {
            try {
                EnvironmentHelper::setDefault($name);
            } catch (Exception $e) {
                $this->error('Error: ' . $e->getMessage());
                return 1;
            }

            $this->info('Environment set as default successfully!');
        }

        return 0;
    }
}
