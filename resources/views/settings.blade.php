<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Environment demo</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    @if (Session::has('success'))
        <div class="notification success">
            {{ Session::get('success') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div class="notification error">
            {{ Session::get('error') }}
        </div>
    @endif

    <h1>Environment</h1>

    <p>Default: {{ $default }}</p>

    <form action="{{ route('settings.environment.fork') }}" method="post">
        @csrf

        <h2>Fork environment</h2>

        <div>
            <label for="fork_environment">Enviroment:</label>

            <select name="fork_environment" id="fork_environment">
                @foreach ($environments as $env)
                    <option value="{{ $env }}">{{ $env }}</option>
                @endforeach
            </select>

            @error('fork_environment')
                <p class="help">{{ $message }}</p>
            @enderror
        </div>

        <div>
            <label for="fork_name">Name:</label>

            <input type="text" name="fork_name" id="fork_name">

            @error('fork_name')
                <p class="help">{{ $message }}</p>
            @enderror
        </div>

        <div>
            <button type="submit">Fork</button>
        </div>
    </form>

    <hr>

    <form action="{{ route('settings.environment.default') }}" method="post">
        @csrf

        <h2>Default environment</h2>

        <div>
            <label for="default_name">Name:</label>

            <select name="default_name" id="default_name">
                @foreach ($environments as $env)
                    <option value="{{ $env }}">{{ $env }}</option>
                @endforeach
            </select>

            @error('default_name')
                <p class="help">{{ $message }}</p>
            @enderror
        </div>

        <div>
            <button type="submit">Set as default</button>
        </div>
    </form>

    <hr>

    <form action="{{ route('settings.environment.delete') }}" method="post">
        @csrf

        <h2>Delete environment</h2>

        <div>
            <label for="delete_name">Name:</label>

            <select name="delete_name" id="delete_name">
                @foreach ($environments as $env)
                    <option value="{{ $env }}">{{ $env }}</option>
                @endforeach
            </select>

            @error('delete_name')
                <p class="help">{{ $message }}</p>
            @enderror
        </div>

        <div>
            <button type="submit">Delete</button>
        </div>
    </form>
</body>
</html>
