<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AppSettings extends Model
{
    use HasFactory;

    protected $connection = 'sqlite_app';
    protected $table = 'settings';

    protected $fillable = [
        'environment',
    ];
}
