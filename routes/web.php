<?php

use App\Http\Controllers\SettingsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('settings')->name('settings.')->group(function () {
    Route::get('/', [SettingsController::class, 'index'])->name('index');
    Route::post('/environment/fork', [SettingsController::class, 'forkEnvironment'])->name('environment.fork');
    Route::post('/environment/default', [SettingsController::class, 'setDefaultEnvironment'])->name('environment.default');
    Route::post('/environment/delete', [SettingsController::class, 'deleteEnvironment'])->name('environment.delete');
});
