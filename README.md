# Laravel API Versioning

This is a Proof of Concept that demonstrates a versioning system in a Laravel backend.

## How it works

Initially, there are two databases; an 'app' database that stores basic information about the application, and a 'main' database which stores the model data of the application.

This 'main' database is called an *environment* and is stored in the `/database/environments/` directory.

It is possible to create new environments on the fly. The given environment schema will be duplicated ('forked'), including all its data. After that, it is possible to set this new environment as the default environment.

The `App/GraphQL/Schemas` directory contains dummy schemas. Dynamic schema creation is not a part of this PoC. These schemas will always return the same schema (`_main`).

## Installation

Copy example env file:

```bash
cp .env.example .env
```

Edit `.env` as needed.

Install dependencies:

```bash
composer install
```

Compile static assets:

```bash
npm install && npm run prod
```

Migrate the app database:

```bash
php artisan migrate:fresh --path ./database/migrations/app --database sqlite_app
```

Migrate the main environment:

```bash
php artisan migrate:fresh --seed
```

## Usage

```php
use App\Helpers\EnvironmentHelper;

// fork the main environment into a new environment called 'test'
EnvironmentHelper::fork('main', 'test');
```

```php
use App\Helpers\EnvironmentHelper;

// set the environment called 'test' as default
EnvironmentHelper::setDefault('test');
```

```php
use App\Helpers\EnvironmentHelper;

// delete the environment called 'test'
EnvironmentHelper::delete('test');
```

```php
use App\Helpers\EnvironmentHelper;

// get the default environment
EnvironmentHelper::default();
```

```php
use App\Helpers\EnvironmentHelper;

// get all environments
EnvironmentHelper::getAll();
```

You can try these functions in the simple GUI found at `/settings`. Run `php artisan serve` to serve the application.
