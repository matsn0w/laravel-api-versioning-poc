<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\_main\ {
    Queries\CustomersQuery,
    Types\CustomerType,
};

use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class MainSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                CustomersQuery::class,
            ],
            'mutation' => [],
            'types' => [
                CustomerType::class,
            ],
        ];
    }
}
