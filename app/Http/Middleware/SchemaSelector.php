<?php

namespace App\Http\Middleware;

use App\Helpers\EnvironmentHelper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SchemaSelector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // get default schema
        $default = EnvironmentHelper::default();

        // set default schema
        Config::set('graphql.default_schema', $default);

        return $next($request);
    }
}
