<?php

namespace App\Providers;

use App\Helpers\EnvironmentHelper;
use App\Models\AppSettings;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class EnvironmentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (!AppSettings::first()) {
            return;
        }

        // get the default environment
        $env = EnvironmentHelper::default();

        // set the connection
        EnvironmentHelper::setDatabaseConnection($env);
    }
}
