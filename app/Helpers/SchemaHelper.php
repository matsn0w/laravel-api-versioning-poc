<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Str;

class SchemaHelper
{
    public static function forkSchema(string $environment, string $name): void
    {
        // get the schema files
        $environment_path = self::getSchemaPath($environment);
        $name_path = self::getSchemaPath($name, false);

        // copy the file
        $result = copy($environment_path, $name_path);

        if (!$result) {
            throw new Exception('Something went wrong!');
        }

        // schema file starts with uppercase letter
        $environment_ucfirst = Str::ucfirst($environment);
        $name_ucfirst = Str::ucfirst($name);

        // get the content of the new schema file
        $content = file_get_contents($name_path);

        // rename the class
        $new_content = Str::replace("{$environment_ucfirst}Schema", "{$name_ucfirst}Schema", $content);

        // put the new content in the file
        file_put_contents($name_path, $new_content);
    }

    public static function delete(string $name): bool
    {
        // get the path of the schema
        $schema_path = self::getSchemaPath($name);

        // check if the schema exists
        if (!file_exists($schema_path)) {
            throw new Exception('This schema does not exist!');
        }

        // delete the file
        return unlink($schema_path);
    }

    protected static function getSchemaPath(string $environment, bool $enforce = true): string
    {
        // schema file starts with uppercase letter
        $environment_ucfirst = Str::ucfirst($environment);

        // find the schema directory
        $schemas = app_path('GraphQL/Schemas');

        // build the file path
        $path = "$schemas/{$environment_ucfirst}Schema.php";

        // check if the file exists
        if (!file_exists($path) && $enforce) {
            throw new Exception('This environment does not exist!');
        }

        return $path;
    }
}
