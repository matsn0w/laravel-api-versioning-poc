<?php

namespace App\Http\Controllers;

use App\Helpers\EnvironmentHelper;
use Exception;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $default = EnvironmentHelper::default();
        $environments = EnvironmentHelper::getAll();

        return view('settings', [
            'default' => $default,
            'environments' => $environments,
        ]);
    }

    public function forkEnvironment(Request $request)
    {
        $validated = $request->validate([
            'fork_environment' => ['required'],
            'fork_name' => ['required'],
        ]);

        $env = $validated['fork_environment'];
        $name = $validated['fork_name'];

        try {
            EnvironmentHelper::fork($env, $name);
        } catch (Exception $e) {
            session()->flash('error', $e->getMessage());
            return back();
        }

        session()->flash('success', "Environment '$env' forked into '$name'!");

        return back();
    }

    public function setDefaultEnvironment(Request $request)
    {
        $validated = $request->validate([
            'default_name' => ['required'],
        ]);

        $env = $validated['default_name'];

        try {
            EnvironmentHelper::setDefault($env);
        } catch (Exception $e) {
            session()->flash('error', $e->getMessage());
            return back();
        }

        session()->flash('success', "Environment '$env' set as default!");

        return back();
    }

    public function deleteEnvironment(Request $request)
    {
        $validated = $request->validate([
            'delete_name' => ['required'],
        ]);

        $env = $validated['delete_name'];

        try {
            EnvironmentHelper::delete($env);
        } catch (Exception $e) {
            session()->flash('error', $e->getMessage());
            return back();
        }

        session()->flash('success', "Environment '$env' deleted!");

        return back();
    }
}
