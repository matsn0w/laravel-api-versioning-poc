<?php

declare(strict_types=1);

namespace App\GraphQL\_main\Types;

use App\Models\Customer;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CustomerType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Customer',
        'description' => 'A simple Customer type.',
        'model' => Customer::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'The ID of the Customer',
            ],

            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the Customer',
            ],

            'balance' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The balance of the Customer',
            ],

            'age' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The age of the Customer',
            ],
        ];
    }
}
