<?php

declare(strict_types=1);

namespace App\GraphQL\_main\Queries;

use Closure;
use App\Models\Customer;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CustomersQuery extends Query
{
    protected $attributes = [
        'name' => 'customers',
        'description' => 'Get all Customers.'
    ];

    public function type(): Type
    {
        return Type::listOf(
            Type::nonNull(GraphQL::type('Customer'))
        );
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return Customer::all();
    }
}
